/**
 *
 * @author gmendez
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;        
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.*;  // GL constants
import static com.jogamp.opengl.GL2.*; // GL2 constants
import static com.jogamp.opengl.GL2ES3.GL_QUADS;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * JoglBase Programa Plantilla (GLCanvas)
 */
@SuppressWarnings("serial")
public class JoglBase extends GLCanvas implements GLEventListener, KeyListener  {
   // Define constants for the top-level container
   private static String TITLE = "Plantilla Base java open gl";  // window's title
   private static final int CANVAS_WIDTH = 640;  // width of the drawable
   private static final int CANVAS_HEIGHT = 480; // height of the drawable
   private float anglePyramid=0;
   private static final int FPS = 24; // animator's target frames per second
   private static final float factInc = 5.0f; // animator's target frames per second
   private float fovy = 45.0f;
   
   private GLU glu;  // for the GL Utility
   private GLUT glut;
   
   float rotacion=0.0f;
   float despl=0.0f;
   float despX=0.0f;
   float despZ=0.0f;
   
   // Posicion de la luz.
  float lightX=1f;
  float lightY=1f;
  float lightZ=1f;
  float dLight=0.05f;
   
   Texture pared,cara,MolinoB,aspa,madera,tejas,trac1,trac2,trac3,ventana,rosa;
    
   // Material  y luces.
  final float ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
  final float position[] = { lightX, lightY, lightZ, 1.0f };
  final float mat_diffuse[] = { 0.6f, 0.6f, 0.6f, 1.0f };
  final float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  final float mat_shininess[] = { 50.0f };

  final float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
  final float[] colorWhite  = {1.0f,1.0f,1.0f,1.0f};
  final float[] colorGray   = {0.4f,0.4f,0.4f,1.0f};
  final float[] colorDarkGray = {0.2f,0.2f,0.2f,1.0f};
  final float[] colorRed    = {1.0f,0.0f,0.0f,1.0f};
  final float[] colorGreen  = {0.0f,1.0f,0.0f,1.0f};
  final float[] colorBlue   = {0.0f,0.0f,0.6f,1.0f};
  final float[] colorYellow = {1.0f,1.0f,0.0f,1.0f};
  final float[] colorLightYellow = {.5f,.5f,0.0f,1.0f};
  final float[] colorDim = {0.6f,07f,0.9f};
  final float[] colorCiel = {0.6f,0.4f,0.6f};
  final float[] colorROp = {0.7f,0.7f,0.8f};
  final float[] colorNar = {0.9f,0.3f,0.1f};
  
   /** The entry main() method to setup the top-level container and animator */
   public static void main(String[] args) {
      // Run the GUI codes in the event-dispatching thread for thread safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            // Create the OpenGL rendering canvas
            GLCanvas canvas = new JoglBase();
            canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
 
            // Create a animator that drives canvas' display() at the specified FPS.
            final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
 
            // Create the top-level container
            final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
            JPanel panel1 = new JPanel();
            
            FlowLayout fl = new FlowLayout();
            frame.setLayout(fl);
            
            panel1.add(canvas);
            frame.getContentPane().add(panel1);
            
            frame.addKeyListener((KeyListener) canvas);
            
            frame.addWindowListener(new WindowAdapter() {
               @Override
               public void windowClosing(WindowEvent e) {
                  // Use a dedicate thread to run the stop() to ensure that the
                  // animator stops before program exits.
                  new Thread() {
                     @Override
                     public void run() {
                        if (animator.isStarted()) animator.stop();
                        System.exit(0);
                     }
                  }.start();
               }
            });
            
            frame.addComponentListener(new ComponentAdapter(){
                    public void componentResized(ComponentEvent ev) {
                            Component c = (Component)ev.getSource();
                            // Get new size
                            Dimension newSize = c.getSize();                            
                            panel1.setSize(newSize);                                                        
                            canvas.setSize(newSize);                            
                    }   
            });
                        
            frame.setTitle(TITLE);
            frame.pack();
            frame.setVisible(true);
            animator.start(); // start the animation loop
         }
      });
   }
 
   /** Constructor to setup the GUI for this Component */
   public JoglBase() {
      this.addGLEventListener(this);
      this.addKeyListener(this);
   }
 
   // ------ Implement methods declared in GLEventListener ------
 
   /**
    * Called back immediately after the OpenGL context is initialized. Can be used
    * to perform one-time initialization. Run only once.
    */
   @Override
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                        // get GL Utilities
      glut = new GLUT();
      gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
      //gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting  
      
      setSomeWhiteMaterial( gl, GL.GL_COMPRESSED_TEXTURE_FORMATS );
      
      // Alguna luz de ambiente global.
      //gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, 
			// this.ambient, 0 );
      
      // First Switch the lights on.
      gl.glEnable( GL2.GL_LIGHTING );
      //gl.glDisable( GL2.GL_LIGHTING );
      gl.glEnable( GL2.GL_LIGHT0 );
      gl.glEnable( GL2.GL_LIGHT1 );
      //gl.glEnable( GL2.GL_LIGHT2 );
      //gl.glEnable( GL2.GL_LIGHT3 ); 
      //gl.glEnable( GL2.GL_LIGHT4 ); // Posicional en Origen


      // Light 0.
      //	      
      //
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, colorBlue, 0 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colorBlue, 0 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, colorWhite, 0 );	

      // Light 1.
      //
      
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_SPECULAR, colorBlue, 0 );
      //gl.glLightfv( GL.GL_LIGHT1, GL.GL_SPECULAR, colorRed, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT1, GL2.GL_CONSTANT_ATTENUATION, 0.5f );

      // Light 2.
      //
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_AMBIENT, colorDim, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_DIFFUSE, colorNar, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_SPECULAR, colorNar, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT2, GL2.GL_CONSTANT_ATTENUATION, 0.8f );

      // Light 3.
      //
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_DIFFUSE, colorDim, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT3, GL2.GL_CONSTANT_ATTENUATION, 0.3f );

      // Light 4.
      //
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_AMBIENT, colorWhite, 0 );
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_DIFFUSE, colorWhite, 0 );
      //gl.glLightfv( GL2.GL_LIGHT4, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf(GL2.GL_LIGHT4, GL2.GL_CONSTANT_ATTENUATION, .3f);
     
      this.pared=this.loadTexture("src\\imagen\\pared.jpg");
      this.cara=this.loadTexture("src\\imagen\\cara.jpg");
      this.MolinoB=this.loadTexture("src\\imagen\\MolinoB.jpg");
      this.aspa=this.loadTexture("src\\imagen\\aspa.jpg");
      this.madera=this.loadTexture("src\\imagen\\madera.jpg");
      this.tejas=this.loadTexture("src\\imagen\\tejas.png");
      this.trac1=this.loadTexture("src\\imagen\\trac1.jpg");
      this.trac2=this.loadTexture("src\\imagen\\trac2.jpg");
      this.trac3=this.loadTexture("src\\imagen\\trac3.png");
      this.ventana=this.loadTexture("src\\imagen\\ventana.jpg");
      this.rosa=this.loadTexture("src\\imagen\\rosa.jpg");

      
      gl.glEnable(GL2.GL_TEXTURE_2D);
      gl.glEnable(GL2.GL_BLEND);
   }
 
   Texture loadTexture(String imageFile) {
        Texture text1 = null;
        try {
            BufferedImage buffImage = ImageIO.read(new File(imageFile));
            text1 = AWTTextureIO.newTexture(GLProfile.getDefault(), buffImage, false);
        } catch (IOException ioe) {
            System.out.println("Problema al cargar el archivo " + imageFile);
        }
        return text1;
    }
   
   /////////////// Define Material /////////////////////

  public void setLightSphereMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorYellow, 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorYellow, 0 );
      //gl.glMaterialfv( face, GL.GL_EMISSION, colorLightYellow , 0 );
      //gl.glMaterialfv( face, GL.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeMaterial( GL2 gl, int face, float rgba[], int offset )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SHININESS, rgba, offset );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, mat_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, mat_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_SHININESS, mat_shininess, 0 );
    }

  public void setSomeWhiteMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorWhite , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeDarkGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorDarkGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }


  public void setSomeYellowMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlack , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorLightYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorYellow , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 5 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeBlueMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlue , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorBlue, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorBlue , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeRedMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorRed , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGreenMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGreen , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGreen , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 10 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorDarkGray , 0 );
    }
   
   /**
    * Call-back handler for window re-size event. Also called when the drawable is
    * first set to visible.
    */
   @Override
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
 
      if (height == 0) height = 1;   // prevent divide by zero
      float aspect = (float)width / height;
 
      // Set the view port (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);

      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      glu.gluPerspective(fovy, aspect, 0.1, 50.0); // fovy, aspect, zNear, zFar
      
      /*
      // Enable the model-view transform
      gl.glMatrixMode(GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
      */
   }
 
   /**
    * Called back by the animator to perform rendering.
    */
   @Override
   public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();  // reset the model-view matrix
        
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        
        glu.gluLookAt(2.0f, 2.0f, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  
        
        /*gl.glColor3f(0.0f,0.0f,1.0f);*/
        
        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(-100.0f,0.0f,0.0f);
            gl.glVertex3f(100.0f,0.0f,0.0f);
        gl.glEnd();

        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(0.0f,-100.0f,0.0f);
            gl.glVertex3f(0.0f,100.0f,0.0f);
        gl.glEnd();

        gl.glBegin(GL2.GL_LINES);
            gl.glVertex3f(0.0f,0.0f,-100.0f);
            gl.glVertex3f(0.0f,0.0f,100.0f);
        gl.glEnd();
        
        gl.glLoadIdentity();
        
        glu.gluLookAt(2.0f, 2.0f, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        gl.glTranslatef(this.despX,0.0f,this.despZ);
         
        //gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
        
        /*Cuadrado 1*/   

        //trac2
            this.trac2.bind(gl);
            this.trac2.enable(gl);
        gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f(1.0f, 0.00392156862f, 0.00392156862f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(0.0f, 1.0f, 0f); 
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(1.0f, 1.0f, 0f);
            gl.glTexCoord2f(1f, 1f);//im2            
            gl.glVertex3f(1.0f, 2.0f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0.0f, 2.0f, 0f);
            gl.glEnd();
            
            /*----------*/
            //trac2
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f(1f, 0.0f, 0.0f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(0.0f, 1.0f, -.8f);
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(1.0f, 1.0f, -.8f);
            gl.glTexCoord2f(1f, 1f);//im2
            gl.glVertex3f(1.0f, 2.0f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0.0f, 2.0f, -.8f); 
            gl.glEnd();
                   
            this.ventana.bind(gl);
            this.ventana.enable(gl);
            /*Ventans*/
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.54117647058f,0.54117647058f,0.54117647058f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(0.2f, 1.9f, 0f);
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(.2f, 1.6f, 0f);
            gl.glTexCoord2f(1f, 1f);//im2            
            gl.glVertex3f(.7f, 1.6f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.7f, 1.9f, 0f); 
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.54117647058f,0.54117647058f,0.54117647058f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(0.2f, 1.9f, -.82f);
            gl.glTexCoord2f(1f, 0);//im3            
            gl.glVertex3f(.2f, 1.6f, -.82f);
            gl.glTexCoord2f(1f, 1f);//im2            
            gl.glVertex3f(.7f, 1.6f, -.82f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.7f, 1.9f, -.82f); 
            gl.glEnd();

            
            
            this.ventana.bind(gl);
            this.ventana.enable(gl);
            //gl.glTexCoord2f(0, 1f);//im1
       //gl.glTexCoord2f(1f, 1f);//im2
       //gl.glTexCoord2f(1f, 0);//im3
       //gl.glTexCoord2f(0, 0);//im4
            /*espejo frontal*/
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.54117647058f,0.54117647058f,0.54117647058f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(1.0f, 1.0f, 0f); 
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(1.0f, 1.0f, -.8f);
            gl.glTexCoord2f(1f, 1f);//im2            
            gl.glVertex3f(1.0f, 2.0f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1.0f, 2.0f, 0f); 
            gl.glEnd();
            
            /*trasero*/
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (1f,0f,0f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(0.0f, 1.0f, 0f); 
            gl.glTexCoord2f(1f, 0);//im3
            gl.glVertex3f(0.0f, 1.0f, -.8f);
            gl.glTexCoord2f(1f, 1f);//im2            
            gl.glVertex3f(0.0f, 2.0f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0.0f, 2.0f, 0f);
            gl.glEnd();

            /*Cuadrado 2*/
            this.trac2.bind(gl);
            this.trac2.enable(gl);
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f(1f, 0f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(1.0f, 1.5f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*b*/ gl.glVertex3f(2.50f, 1.5f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*c*/ gl.glVertex3f(2.50f, .6f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(1.0f, .6f, 0f);  
           gl.glEnd();
            
                  
           //trac3
           gl.glBegin(GL2.GL_QUADS);
           gl.glColor3f(1f, 0f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(1.0f, 1.5f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*b*/ gl.glVertex3f(2.50f, 1.5f, -.8f); 
           gl.glTexCoord2f(0, 1f);//im1           
           /*c*/ gl.glVertex3f(2.50f, .6f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(1.0f, .6f, -.8f);
           gl.glEnd();
           
           /*Morado*/
           gl.glBegin(GL2.GL_QUADS);
           gl.glColor3f (1f,0f,0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(1.0f, 1.5f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*b*/ gl.glVertex3f(1.0f, 1.5f, -.8f); 
                gl.glTexCoord2f(0, 1f);//im1           
           /*c*/ gl.glVertex3f(1.0f, .6f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(1.0f, .6f, 0f); 
           gl.glEnd();
            
           /*Morado*/
           gl.glBegin(GL2.GL_QUADS);
           gl.glColor3f (1f,0f,0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(2.5f, 1.5f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*b*/ gl.glVertex3f(2.5f, 1.5f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*c*/ gl.glVertex3f(2.5f, .6f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(2.5f, .6f, 0f);
           gl.glEnd();
           
            gl.glBegin(GL2.GL_QUADS); 
           gl.glColor3f (0.72156862745f,0f,0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(1.0f, 1.5f, 0f);
           gl.glTexCoord2f(0, 1f);//im1
           /*b*/ gl.glVertex3f(1.0f, 1.5f, -.8f); 
           gl.glTexCoord2f(0, 1f);//im1           
           /*c*/ gl.glVertex3f(2.5f, 1.5f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(2.5f, 1.5f, 0f);
           gl.glEnd();
           
           gl.glBegin(GL2.GL_QUADS); 
           gl.glColor3f (0.7f,0.7f,0.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*a*/ gl.glVertex3f(1.0f, .6f, 0f); 
           gl.glTexCoord2f(0, 1f);//im1           
           /*b*/ gl.glVertex3f(1.0f, .6f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*c*/ gl.glVertex3f(2.5f, .6f, -.8f);
           gl.glTexCoord2f(0, 1f);//im1
           /*d*/ gl.glVertex3f(2.5f, .6f, 0f);
           gl.glEnd();
   
           
            this.madera.bind(gl);
            this.madera.enable(gl);
            /*Parte de arriba*/
            //trac1
            gl.glBegin(GL2.GL_QUADS);           
            gl.glColor3f(0f, 0f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1.20f, 2.20f, 0f);
            gl.glTexCoord2f(0, 1f);//im1            
            gl.glVertex3f(-.40f, 2.20f, 0f); 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(-.40f, 2f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1.20f, 2f, 0f); 
            gl.glEnd();
 
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f(0f, 0f, 0f);
            gl.glVertex3f(1.20f, 2.20f, -.8f);            
            gl.glVertex3f(-.40f, 2.20f, -.8f);            
            gl.glVertex3f(-.40f, 2f, -.8f);
            gl.glVertex3f(1.20f, 2f, -.8f);    
            gl.glEnd();
           
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
           /*a*/ gl.glVertex3f(1.20f, 2.2f, 0f);            
           /*b*/ gl.glVertex3f(1.20f, 2.2f, -.8f);            
           /*c*/ gl.glVertex3f(1.20f, 2f, -.8f);
           /*d*/ gl.glVertex3f(1.20f, 2f, 0f);
           gl.glEnd();
                      
           gl.glBegin(GL2.GL_QUADS);           
            gl.glColor3f (0f,0f,0f);
           /*a*/ gl.glVertex3f(-.40f, 2.2f, 0f);            
           /*b*/ gl.glVertex3f(-.40f, 2.2f, -.8f);            
           /*c*/ gl.glVertex3f(1.20f, 2.2f, -.8f);
           /*d*/ gl.glVertex3f(1.20f, 2.2f, 0f);
           gl.glEnd();
           
           gl.glBegin(GL2.GL_QUADS);
           gl.glColor3f (0f,0f,0f);
           /*a*/ gl.glVertex3f(-.40f, 2f, 0f);            
           /*b*/ gl.glVertex3f(-.40f, 2f, -.8f);            
           /*c*/ gl.glVertex3f(-.40f, 2.2f, -.8f);
           /*d*/ gl.glVertex3f(-.40f, 2.2f, 0f);
           gl.glEnd();
                    
           this.madera.bind(gl);
            this.madera.enable(gl);
           //llanta
        //gl.glVertex3f(0f,.25f, 0f);//8
           gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f, 0f,0f);//1
            gl.glTexCoord2f(0, 1f);//im1            
            gl.glVertex3f(.75f, 0f,0f);//2 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f, .25f,0f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, 0f);//4
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, 0f);//5
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,1f, 0f);//6
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.75f, 0f);//7
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, 0f);//8  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f, 0f,-.2f);//1 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f, 0f,-.2f);//2 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f, .25f,-.2f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.2f);//4
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.2f);//5
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,1f, -.2f);//6
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.75f, -.2f);//7
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.2f);//8  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f, 0f,-.6f);//1 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f, 0f,-.6f);//2  
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f, .25f,-.6f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.6f);//4
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.6f);//5
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,1f, -.6f);//6
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.75f, -.6f);//7
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.6f);//8 
            gl.glEnd();
                        
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f, 0f,-.8f);//1 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f, 0f,-.8f);//2 
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f, .25f,-.8f);//3
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.8f);//4
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.8f);//5
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,1f, -.8f);//6
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.75f, -.8f);//7
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.8f);//8  
            gl.glEnd();
            
            /*------------------*/
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, 0f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,0f, 0f); 
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.2f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.2f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.2f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,0f, -.2f);
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.8f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,0f, -.8f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(0f,.25f, -.6f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.75f,1f, -.6f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(1f,.75f, -.6f);
            gl.glTexCoord2f(0, 1f);//im1
            gl.glVertex3f(.25f,0f, -.6f); 
            gl.glEnd();  
            
            /*GRANERO*/
            this.pared.bind(gl);
            this.pared.enable(gl);
            gl.glColor3f(.3f,.8f,.9f);  
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0f, 0f);
            gl.glVertex3f(-5f, 0f,0f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-1f, 0f,0f);   
            gl.glTexCoord2f(1f, 1f);//
            gl.glVertex3f(-1f, 3f,0f);
            gl.glTexCoord2f(0f, 1f);
            gl.glVertex3f(-5f, 3f,0f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-5f, 0f,-2.0f);//1
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-1f, 0f,-2.0f);//2  
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-1f, 3f,-2.0f);//3
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-5f, 3f,-2.0f);//4
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.72156862745f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-1f, 0f,0f);//1
            gl.glTexCoord2f(1f, 0);          
            gl.glVertex3f(-1f, 0f,-2f);//2  
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-1f, 3f,-2f);//3
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-1f, 3f,0f);//4
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.72156862745f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-5f, 0f,0f);//1
            gl.glTexCoord2f(1f, 0);           
            gl.glVertex3f(-5f, 0f,-2f);//2  
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-5f, 3f,-2f);//3
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-5,3f,0f);//4  
            gl.glEnd();
            
            
            this.tejas.bind(gl);
            this.tejas.enable(gl);
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.72156862745f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-5f, 3f,0f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-3f, 3f,0f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-1f, 3f,0f);
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-3f,4.50f,0f);  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.72156862745f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-5f, 3f,-2f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-3f, 3f,-2f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-1f, 3f,-2f);
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-3f,4.50f,-2f);  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (1f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-1f, 3f,0f); 
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-1f, 3f,-2f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-3f,4.5f,-2f);
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-3f,4.5f,0f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (1f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-5f, 3f,0f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-5f, 3f,-2f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-3f,4.5f,-2f);
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-3f,4.5f,0f); 
            gl.glEnd();
            
            
            this.madera.bind(gl);
            this.madera.enable(gl);
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glVertex3f(1.80f, 0.4f,0f);//1            
            gl.glVertex3f(1.80f, 0.20f,0f);//2         
            gl.glVertex3f(2f, 0f,0f);//3
            gl.glVertex3f(2.30f,0f, 0f);//4
            gl.glVertex3f(2.50f,.20f, 0f);//5
            gl.glVertex3f(2.50f,.40f, 0f);//6
            gl.glVertex3f(2.30f,.6f, 0f);//7
            gl.glVertex3f(2f,.6f, 0f);//8  
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glVertex3f(2f,.6f, 0f);//5
            gl.glVertex3f(1.80f,.4f, 0f);//6
            gl.glVertex3f(2.30f,.0f, 0f);//7
            gl.glVertex3f(2.50f,.20f, 0f);//8  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glVertex3f(1.80f, 0.4f,-.8f);//1            
            gl.glVertex3f(1.80f, 0.20f,-.8f);//2         
            gl.glVertex3f(2f, 0f,-.8f);//3
            gl.glVertex3f(2.30f,0f, -.8f);//4
            gl.glVertex3f(2.50f,.20f, -.8f);//5
            gl.glVertex3f(2.50f,.40f, -.8f);//6
            gl.glVertex3f(2.30f,.6f, -.8f);//7
            gl.glVertex3f(2f,.6f, -.8f);//8  
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glVertex3f(2f,.6f, -.8f);//5
            gl.glVertex3f(1.80f,.4f, -.8f);//6
            gl.glVertex3f(2.30f,.0f, -.8f);//7
            gl.glVertex3f(2.50f,.20f, -.8f);//8 
            gl.glEnd();
            
            /*------*/
            this.MolinoB.bind(gl);
            this.MolinoB.enable(gl);
            gl.glBegin(GL2.GL_QUADS); 
            gl.glColor3f (0.68503937007f,0.31496062992f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(6f,0f, 2f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(6.5f,3f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(7.43f,3f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(8f,.0f, 2f);//8 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);            
            gl.glColor3f (0.68503937007f,0.31496062992f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(6f,0f, 0f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(6.5f,3f, 0f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(7.43f,3f, 0f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(8f,.0f, 0f);//8  
            gl.glEnd();

            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.31496062992f,0.14566929133f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(6f,0f, 0f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(6f,0f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(6.5f,3f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(6.5f,3f, 0f);//8 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.31496062992f,0.14566929133f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(8f,0f, 0f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(8f,0f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(7.43f,3f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(7.43f,3f, 0f);//8
            gl.glEnd();
            
            
            this.aspa.bind(gl);
            this.aspa.enable(gl);
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(7.15f,2f, 2f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(8f,2f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(8f,1.8f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(7.15f,1.8f, 2f);//8 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0f,0f,0f);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(5.9f,2f, 2f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(5.9f,1.8f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(6.87f,1.8f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(6.87f,2f, 2f);//8  
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(7.12f,3.15f, 2f);//6
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(6.9f,3.15f, 2f);//5
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(6.9f,2.3f, 2f);//8 
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(7.12f,2.3f, 2f);//7
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(6.9f,1.60f, 2f);//5
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(7.12f,1.60f, 2f);//6
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(7.12f,.87f, 2f);//7
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(6.9f,.87f, 2f);//8  
            gl.glEnd();
            
            
            this.madera.bind(gl);
            this.madera.enable(gl);
            //gl.glTexCoord2f(0, 1f);//im1
       //gl.glTexCoord2f(1f, 1f);//im2
       //gl.glTexCoord2f(1f, 0);//im3
       //gl.glTexCoord2f(0, 0);//im4
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(6.82f,2.30f, 2f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(7.23f,2.30f, 2f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(7.23f,1.56f, 2f);
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(6.82f,1.56f, 2f);
            gl.glEnd();
            
            //cerdo
            this.rosa.bind(gl);
            this.rosa.enable(gl);
       
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.95275590551f,0.59842519685f,0.40944881889f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.95275590551f,0.59842519685f,0.40944881889f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 1f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 1f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glColor3f (0.95275590551f,0.59842519685f,0.40944881889f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 1f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 1f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.5f, 1f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1.5f, 1f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,1f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,1f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,1f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 1.8f);
            gl.glEnd();
                       
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f,1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.5f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.8f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,0f, 1.4f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f, 1.4f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.8f,1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,0f, 1.1f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f,1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f, 1.1f);
            gl.glEnd();
             
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.30f,.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.30f,0f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.30f,.5f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.5f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 1.8f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.30f,0f, 1.8f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.7f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,1.7f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,.8f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f,2f);
            gl.glEnd();
            
            
            this.cara.bind(gl);
            this.cara.enable(gl);    
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);
            gl.glVertex3f(-1f,1.7f, 2.5f);
            gl.glTexCoord2f(1f, 0);
            gl.glVertex3f(-1.8f,1.7f, 2.5f);
            gl.glTexCoord2f(1f, 1f);
            gl.glVertex3f(-1.8f,.8f, 2.5f); 
            gl.glTexCoord2f(0, 1f);
            gl.glVertex3f(-1f,.8f,2.5f);
            gl.glEnd();
            
            this.rosa.bind(gl);
            this.rosa.enable(gl);
           //cuerpo
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,.8f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,1.7f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,1.7f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,.8f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.7f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.7f, 2f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 2f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,1.7f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.7f, 2.5f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.8f,1.7f, 2.5f); 
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,1.7f, 2.5f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f,2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 1.8f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f,2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f, 1.8f);
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f,2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,.8f, 2f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,.8f, 1.8f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f, 1.8f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.3f,0f, 1.1f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1.7f,0f, 1.1f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-1f,0f, 1.1f); 
            gl.glEnd();
            
            gl.glBegin(GL2.GL_QUADS);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f,1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.8f, 1.4f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,.8f, 1.1f);
            gl.glTexCoord2f(0, 0);//im4
            gl.glVertex3f(-2f,0f, 1.1f);
            gl.glEnd();
            
            gl.glEnd(); 
   
        
        this.rotacion+=5.0f;
        if (this.rotacion>360){
            this.rotacion = 0;
        }
        
        System.out.printf("Rotacion %f \n", this.rotacion);
        
            
        gl.glFlush();
            
   }

   

   /**
    * Called back before the OpenGL context is destroyed. Release resource such as buffers.
    */
   @Override
   public void dispose(GLAutoDrawable drawable) { }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int codigo = e.getKeyCode();
        
        System.out.println("codigo presionado = "+codigo);
        
        switch (codigo){                     
            case KeyEvent.VK_LEFT:
                 this.despX-=0.2f;
                 break;
            case KeyEvent.VK_RIGHT:
                 this.despX+=0.2f;
                 break;  
            case KeyEvent.VK_DOWN:    
                 this.despZ+=0.2f;
                 break;                 
            case KeyEvent.VK_UP:
                 this.despZ-=0.2f;
                 break;
            case KeyEvent.VK_R:
                 this.rotacion+=5.0f;
                 break;                 
        }
        System.out.println("despX ="+this.despX+" - "+"despZ ="+this.despZ); 
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }



}